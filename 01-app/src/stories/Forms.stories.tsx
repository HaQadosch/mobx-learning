import * as React from 'react'
import { ComponentMeta } from '@storybook/react'
import { FormSizeDemo } from '../Form'
import 'antd/dist/antd.css'

export default {
  title: 'Example/Forms',
  component: FormSizeDemo,
  parameters: {
    viewport: {
      viewports: {
        app: {
          name: 'app',
          styles: {
            width: '458px',
            height: '300px',
          },
        }
      },
      defaultViewport: 'app'
    },
  },
  argTypes: {}
} as ComponentMeta<typeof FormSizeDemo>

export const Primary = {
  args: {
    primary: true,
    label: 'Button'
  }
}
