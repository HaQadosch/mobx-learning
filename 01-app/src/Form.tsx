import * as React from 'react'
import {
  Form,
  Input,
  Button,
  Select,
  DatePicker,
  InputNumber,
  Row,
  Col,
} from 'antd'
import 'antd/dist/antd.css'

export const FormSizeDemo = () => {

  return (
    <Form
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 14 }}
      layout='horizontal'
      onValuesChange={() => { console.log('value changed') }}
      size='small'
    >
      <Form.Item>
        <Input value='this is the last comment recorded' />
      </Form.Item>
      <Form.Item>
        <Row gutter={8} >
          <Col span={8} >
            <Form.Item >
              <Select>
                <Select.Option value='personal'>personal</Select.Option>
                <Select.Option value='general'>general</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <InputNumber />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <DatePicker />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <Button>Save</Button>
        </Form.Item>
      </Form.Item>
    </Form>
  )
}
